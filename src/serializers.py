# serializers.py
from rest_framework import serializers
from .models import Currency, Payment
from django.contrib.auth.models import User

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'

class PaymentSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
    fullname = serializers.SerializerMethodField()
    currency_code = serializers.CharField(source='currency.code')
    username= serializers.CharField(source='user.username')
    class Meta:
        model = Payment
        fields = ['reference_code','amount','currency','user',"fullname","currency_code","username"]

    def get_fullname(self, obj):
        return f"{obj.user.first_name} {obj.user.last_name}"
