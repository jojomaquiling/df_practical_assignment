# views.py
from rest_framework import viewsets
from .models import Currency, Payment
from .serializers import CurrencySerializer, PaymentSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.db.models import Sum
import logging 
import django
from datetime import datetime
from django.utils import timezone


logger = logging.getLogger(__name__)

class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = [IsAuthenticated]


class PaymentViewSet(viewsets.ModelViewSet):
    queryset = Payment.objects.select_related('user', 'currency')
    serializer_class = PaymentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        reference_code = self.request.query_params.get('reference_code', None)
        currency = self.request.query_params.get('currency', None)
        if reference_code:
            queryset = queryset.filter(reference_code=reference_code)
        if currency:
            queryset = queryset.filter(currency__code=currency)
        return queryset

    def create(self, request, pk=None):
        user = self.request.user.id
        current_date = datetime.now().date()
        total_value  = Payment.objects.filter(currency=request.POST.get("currency"), user_id = self.request.user.id, created_date__date=current_date).aggregate(total=Sum('amount'))
        logger.info("Total value",total_value)
        if (not total_value['total']==None):
            if (total_value['total'] + float(request.POST.get("amount")) > 5000):
                 return Response({"error":"Exceeds payment, total pay for the day : {}".format(total_value["total"])})

        p = None
        try:       
            payment = {
                "reference_code": request.POST.get('reference_code'),
                "amount": request.POST.get('amount'),
                "currency": Currency.objects.get(id=request.POST.get("currency")),
                "user_id": self.request.user.id 
                }

            logger.debug(payment)
            p = Payment(**payment)
            p.save()
        except django.db.utils.IntegrityError as ex:
            return Response({"error": "Duplicate reference code {}".format(request.POST.get('reference_code'))}) 
        except Currency.DoesNotExist as ex:
            return Response({"error": "{} : Currency Does not Exist".format(request.POST.get('currency'))}) 

        return Response({'reference_code': p.reference_code,"status":"Payment is added", "total" : total_value['total'] })


    @action(detail=True, methods=['post'])
    def mark_paid(self, request, pk=None):
        try:
            payment = Payment.objects.filter(id=pk).first()
            if not payment:
                return Response({"error": f"ID # {pk} : There is no such id "})
            payment.is_paid = True
            payment.paid_date = timezone.now()
            payment.save()
            return Response({'message': f'Payment with id {pk} has been marked as paid.'})
        except Payment.DoesNotExist as ex:
            return Response({'error': "Reference code does not exist"})

    @action(detail=True, methods=['post'])
    def pay_now(self, request, pk=None):
        queryset = super().get_queryset()
        current_date = datetime.now().date()
        queryset = queryset.filter(user=self.request.user, created_at__date=current_date)
        total_amount = queryset.objects.aggregate(total=Sum('price'))
        return Response({'total_amount': total_amount})





